package com.company;

import java.util.Scanner;

public class Main {

    static Scanner input;
    static String nama;
    static String nomorKK;
    static String nomorNIK;
    static String noHP;
    static String domisili;
    static String email;
    static int kelas;


    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println("==================================================");
        System.out.println(" Halo! Selamat datang di aplikasi registrasi BPJS");
        System.out.println("==================================================\n");
        System.out.println("Silahkan lakukan penginputan data pada formulir dibawah ini!\n");

        input = new Scanner(System.in);
        System.out.print("Nama Lengkap  : ");
        nama = input.nextLine();

        input = new Scanner(System.in);
        System.out.print("Nomor KK      : ");
        nomorKK = input.nextLine();

        input = new Scanner(System.in);
        System.out.print("Nomor NIK     : ");
        nomorNIK = input.nextLine();

        input = new Scanner(System.in);
        System.out.print("No HP aktif   : ");
        noHP = input.nextLine();

        input = new Scanner(System.in);
        System.out.print("E-mail aktif  : ");
        email = input.nextLine();

        input = new Scanner(System.in);
        System.out.print("Domisili      : ");
        domisili = input.nextLine();


        System.out.println("------------------------------------------------------------------");
        System.out.println("Sekarang anda diminta untuk memilih kelas BPJS");
        System.out.println("Berikut biaya iuran bulanan BPJS:");
        System.out.println("⚫ Rp42.000 per orang untuk setiap bulan untuk perawatan kelas 3");
        System.out.println("⚫ Rp110.000 per orang untuk setiap bulan untuk perawatan kelas 2");
        System.out.println("⚫ Rp160.000 per orang untuk setiap bulan untuk perawatan kelas 1");
        System.out.println("jawab dengan;kelas 1, kelas 2 atau kelas 3 sesuai pilihan anda");

        input = new Scanner(System.in);
        System.out.print("Kelas pilihan : ");
        kelas = input.nextInt();
        User users =  new User(nama, nomorKK, nomorNIK, noHP, domisili, email, kelas);

        System.out.println("===================================================================");
        System.out.println("\n\nAnda baru saja menyelesaikan tahapan pertama pendaftaran \nBPJS Kesehatan.");
        System.out.println("Data atas nama " + users.getNama() + ", dengan nomor KK " +  users.getNomorKK() + " \nakan segera diproses.");
        System.out.println("Tahap selanjutnya akan kami sampaikan dalam bentuk email \nkepada alamat " + users.getEmail());
        System.out.println("Informasi lebih lanjut, hubungi call center kami di 1500 400 \nTerima kasih.");

    }

}
