package com.company;

public class User {

    public User(String name, String nomorKK, String nomorNIK, String NoHP, String domisili, String email, int kelas) {
        this.nama = name;
        this.nomorKK = nomorKK;
        this.nomorNIK = nomorNIK;
        this.noHP = NoHP;
        this.domisili = domisili;
        this.email = email;
        this.kelas = kelas;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNomorKK() {
        return nomorKK;
    }

    public void setNomorKK(String nomorKK) {
        this.nomorKK = nomorKK;
    }

    public String getNomorNIK() {
        return nomorNIK;
    }

    public void setNomorNIK(String nomorNIK) {
        this.nomorNIK = nomorNIK;
    }

    public String getNoHP() {
        return noHP;
    }

    public void setNoHP(String noHP) {
        this.noHP = noHP;
    }

    public String getDomisili() {
        return domisili;
    }

    public void setDomisili(String domisili) {
        this.domisili = domisili;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getKelas() {
        return kelas;
    }

    public void setKelas(int kelas) {
        this.kelas = kelas;
    }


    private String nama;
    private String nomorKK;
    private String nomorNIK;
    private String noHP;
    private String domisili;
    private String email;
    private int kelas;

}
